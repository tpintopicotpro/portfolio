window.onload = function () {
  const cards = document.getElementsByClassName('box');
  
  for (element of cards) {
    const childs = element.childNodes;
    childs[1].name = childs[1].href;
    childs[1].href = "#";

    element.addEventListener('click', (e) => {
      if (e.target.parentElement.href.endsWith('#')){
        e.preventDefault();
        const href = e.target.parentElement.name;
        e.target.parentElement.href = href;
      }
    });
  };
}